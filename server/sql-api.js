const { eventsEnum } = require('./events.enum');
const { Pool: DBPool } = require('pg');
class SqlApi {
  constructor({ user, database, password, host, port }) {
    this._pgPool = new DBPool({ user, database, password, host, port });
  }

  [eventsEnum.getAll] = (route) => {
    const sqlQuery = `SELECT * FROM ${route}`;
    return this._requestToDB(sqlQuery);
  };

  [eventsEnum.addRecord] = (route, record) => {
    const columns = `${Object.keys(record)}`;
    const values = Object.values(record);
    const params = values.map((_, index) => `$${index + 1}`);
    const sqlQuery = `INSERT INTO ${route}(${columns}) VALUES (${params})`;

    return this._requestToDB(sqlQuery, values);
  };

  [eventsEnum.editRecord] = (route, editedRecord) => {
    const columns = Object.keys(editedRecord);
    const params = columns.reduce((acc, col, index) => {
      if (col === 'id') return acc;
      return acc.concat(`${col} = $${index + 1}`);
    }, []);
    const values = Object.values(editedRecord);

    const sqlQuery =
      `UPDATE ${route} ` +
      `SET ${params.join()} ` +
      `WHERE id = $${columns.indexOf('id') + 1};`;

    return this._requestToDB(sqlQuery, values);
  };

  [eventsEnum.deleteRecords] = (route, records) => {
    const params = records.map((_, index) => `$${index + 1}`);
    const sqlQuery = `DELETE FROM ${route} WHERE id IN (${params});`;

    return this._requestToDB(sqlQuery, records);
  };

  deleteBy = async (route, column, value) => {
    const selectRequest = `SELECT id FROM ${route} WHERE ${column} IN (${value.join()});`;
    const recordsToDelete = await this._requestToDB(selectRequest);
    if (!recordsToDelete?.length) return;

    const sqlQuery = `DELETE FROM ${route} WHERE id IN (${recordsToDelete
      .map(({ id }) => id)
      .join()});`;

    return this._requestToDB(sqlQuery);
  };

  _requestToDB(sqlQuery, params) {
    return this._pgPool.connect().then((client) => {
      return client
        .query(sqlQuery, params)
        .then((res) => {
          client.release();
          return res.rows;
        })
        .catch((err) => {
          client.release();
          console.log(err.stack);
        });
    });
  }
}

module.exports.SqlApi = SqlApi;
