const { eventsEnum } = require('../events.enum');
const { prepareData } = require('../prepareData');

const getStringValue = (rawString) => rawString?.replace(/"/g, '') || '';

const defaultValidator = (data) => {
  if (!getStringValue(data.name).length || /undefined/.test(data.name))
    return 'The name should not be empty!';
};

const route = 'owners';

function owners({ event, data }, api) {
  const validators = {
    [eventsEnum.addRecord]: defaultValidator,
    [eventsEnum.editRecord]: defaultValidator,
  };

  const error = validators[event]?.(data);

  const handlers = {
    [eventsEnum.getAll]: async () => {
      try {
        const content = await api[eventsEnum.getAll](route);
        return prepareData(content, route, event, 'ok');
      } catch (err) {
        console.log(err);
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.addRecord]: async () => {
      try {
        if (error) throw error;

        const result = await api[eventsEnum.addRecord](route, data);

        return prepareData(result, route, event, 'ok');
      } catch (err) {
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.editRecord]: async () => {
      try {
        if (error) throw error;

        const result = await api[eventsEnum.editRecord](route, data);
        return prepareData(result, route, event, 'ok');
      } catch (err) {
        console.log(err);
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.deleteRecords]: async () => {
      try {
        const result = await api[eventsEnum.deleteRecords](route, data);
        await api.deleteBy('dogs', 'owner', data);

        return prepareData(result, route, event, 'ok');
      } catch (err) {
        return prepareData(null, route, event, 'bad', err);
      }
    },
  };

  return handlers[event]();
}

module.exports.owners = owners;
