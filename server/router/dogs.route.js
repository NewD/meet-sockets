const { eventsEnum } = require('../events.enum');
const { prepareData } = require('../prepareData');

const defaultValidator = (data, isEdit) => {
  if (!data.name?.length || /undefined/.test(data.name))
    return 'The name should not be empty!';
  if (!isEdit && typeof data.owner !== 'number') {
    console.log(data);
    return 'Dogs should have an owner!';
  }
  if (!+data.age) return 'The age should not be empty!';
  if (+data.age < 0) return 'The age should be more than 0!';
  if (+data.age > 30) return 'The age should be less than 30!';

  return '';
};

const route = 'dogs';

function dogs({ event, data }, api) {
  const validators = {
    [eventsEnum.addRecord]: defaultValidator,
    [eventsEnum.editRecord]: (data) =>
      defaultValidator(data, event === eventsEnum.editRecord),
  };

  const error = validators[event]?.(data);

  const handlers = {
    [eventsEnum.getAll]: async () => {
      try {
        const content = await api[eventsEnum.getAll](route);

        return prepareData(content, route, event, 'ok');
      } catch (err) {
        console.log(err);
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.addRecord]: async () => {
      try {
        if (error) throw error;

        const result = await api[eventsEnum.addRecord](route, data);

        return prepareData(result, route, event, 'ok');
      } catch (err) {
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.editRecord]: async () => {
      try {
        if (error) throw error;

        const result = await api[eventsEnum.editRecord](route, data);

        return prepareData(result, route, event, 'ok');
      } catch (err) {
        return prepareData(null, route, event, 'bad', err);
      }
    },

    [eventsEnum.deleteRecords]: async () => {
      try {
        const result = await api[eventsEnum.deleteRecords](route, data);

        return prepareData(result, route, event, 'ok');
      } catch (err) {
        return prepareData(null, route, event, 'bad', err);
      }
    },
  };

  return handlers[event]();
}

module.exports.dogs = dogs;
module.exports.dogsRoute = route;
