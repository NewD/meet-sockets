const { dogs } = require('./router/dogs.route');
const { owners } = require('./router/owners.route');
const messageDelimeter = '--/--';

function parseMessage(rawMessage, api) {
  const messages = rawMessage.split(messageDelimeter).filter((mess) => !!mess);

  return Promise.all(
    messages.map(async (message) => {
      const { route, event, data } = JSON.parse(message);
      const routes = {
        dogs: () => dogs({ event, data }, api),
        owners: () => owners({ event, data }, api),
      };

      return { data: await routes[route](), event };
    })
  );
}

module.exports.parseMessage = parseMessage;
