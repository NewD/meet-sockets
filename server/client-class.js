const { eventsEnum } = require('./events.enum');
const { parseMessage } = require('./parseMessage');
const { SqlApi } = require('./sql-api');

const defaultDbCredentials = {
  user: 'postgres',
  database: 'meet-sockets',
  password: '12345678',
  host: '127.0.0.1',
  port: 5432,
};

const messageDelimeter = '--/--';

class Client {
  constructor(client, updateRestClients, dbCredentials = defaultDbCredentials) {
    this.client = client;
    console.log(
      `Client connect (local address: ${this.client.localAddress}:${this.client.localPort}, ` +
        `remote address: ${this.client.remoteAddress}:${this.client.remotePort}`
    );
    this.api = new SqlApi(dbCredentials);
    this.client.setEncoding('utf-8');
    client.addListener('data', this._handleMessage);

    this.id = client.remoteAddress + client.remotePort;
    this._updateRestClients = () => updateRestClients(this.id);
  }

  update() {
    this.client.write(JSON.stringify({ status: 'update' }) + messageDelimeter);
  }

  _handleMessage = async (data) => {
    try {
      console.log('Received message!');
      const response = await parseMessage(data, this.api);

      response.forEach((el) => {
        this.client.write(el.data + messageDelimeter);
        if (
          [
            eventsEnum.addRecord,
            eventsEnum.editRecord,
            eventsEnum.deleteRecords,
          ].includes(el.event)
        ) {
          this._updateRestClients();
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
}

module.exports.Client = Client;
