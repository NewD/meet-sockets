require('dotenv').config();
const net = require('net');
const { Client } = require('./client-class');
const port = process.env.SERVER_PORT || 9999;
const host = process.env.SERVER_HOST || '192.168.1.215';

let clients = [];

// Create and return a net.Server object, the function will be invoked when client connect to this server.
const server = net.createServer();

const updateRestClients = (currentClientId) => {
  const restClients = clients.filter((cl) => cl.id !== currentClientId);

  restClients.forEach((cl) => cl.update());
};

server.on('connection', function (client) {
  const newClient = new Client(client, updateRestClients);

  clients.push(newClient);
  console.log(`Clients count is ${clients.length}`);

  client.on('end', function () {
    clients = clients.filter((cl) => cl !== cl);
    console.log(`Client disconnect. Clients count is ${clients.length}`);

    server.getConnections(function (err, count) {
      if (!err) {
        console.log('There are %d connections now. ', count - 1);
      } else {
        console.error(JSON.stringify(err));
      }
    });
  });
});

server.listen({ host, port }, function () {
  const serverInfo = server.address();
  console.log(
    'TCP server listen on address: ' + serverInfo.address + ':' + serverInfo.port
  );

  server.on('close', function () {
    console.log('TCP server socket is closed.');
  });

  server.on('error', function (error) {
    console.error(JSON.stringify(error));
  });
});
