const { eventsEnum } = require('./events.enum');

function prepareData(data, route, event, status = 'ok', error) {
  const handlers = {
    [eventsEnum.getAll]: () => ({
      route,
      event: eventsEnum.getAll,
      content: data,
      status,
    }),
    [eventsEnum.addRecord]: () => ({
      event: eventsEnum.addRecord,
      route,
      status,
      error,
    }),
    [eventsEnum.editRecord]: () => ({
      event: eventsEnum.editRecord,
      route,
      status,
      error,
    }),
    [eventsEnum.deleteRecords]: () => ({
      event: eventsEnum.deleteRecords,
      route,
      status,
    }),
  };

  return JSON.stringify(handlers[event]());
}

module.exports.prepareData = prepareData;
