export const eventsEnum = {
  getAll: 'getAll',
  addRecord: 'addRecord',
  editRecord: 'editRecord',
  deleteRecords: 'deleteRecords',
};
