import { eventsEnum } from '../events.enum';

export const api = {
  get: (route, data) => ({ route, event: eventsEnum.getAll, data }),
  add: (route, data) => ({ route, event: eventsEnum.addRecord, data }),
  edit: (route, data) => ({ route, event: eventsEnum.editRecord, data }),
  delete: (route, data) => ({ route, event: eventsEnum.deleteRecords, data }),
};
