import { useCallback, useContext, useEffect, useState } from 'react';
import { closeWsConnection, createWsConnection, sendMessageService } from './services/ws';
import { createContext } from 'react';

const WsContext = createContext(null);

export const WsProvider = ({ children }) => {
  const [socket, setSocket] = useState(null);
  const [isConnected, setIsConnected] = useState(false);
  const [isNeedToUpdate, setIsNeedToUpdate] = useState(false);
  const [response, setResponse] = useState(null);
  const messageDelimeter = '--/--';

  const connect = useCallback(() => {
    setSocket(createWsConnection('ws://localhost:8999', setIsConnected));
  }, []);

  const disconnect = useCallback(() => {
    closeWsConnection(socket);
  }, [socket]);

  useEffect(() => {
    if (!socket) return;
    socket.addEventListener('message', ({ data }) => {
      const messages = data.split(messageDelimeter);

      messages.forEach((mess) => {
        if (!mess) return;
        const parsedData = JSON.parse(mess);

        const { status } = parsedData;
        if (status === 'update') {
          return setIsNeedToUpdate(true);
        }

        setResponse(parsedData);
      });
    });
  }, [socket]);

  const sendMessage = useCallback(
    (data) => {
      sendMessageService(socket, JSON.stringify(data) + messageDelimeter);
    },
    [socket]
  );

  return (
    <WsContext.Provider
      value={{
        connect,
        response,
        disconnect,
        sendMessage,
        isConnected,
        isNeedToUpdate,
        setIsNeedToUpdate,
      }}
    >
      {children}
    </WsContext.Provider>
  );
};

export const useWsContext = () => useContext(WsContext);
