import { MainLayout } from './components/main-layout';
import { WsProvider } from './ws-context';
import { DogsTable } from './components/dogs-table';
import { OwnersTable } from './components/owners-table';
import { CommonDataProvider } from './common-data-context';
import { AuthProvider } from './auth-context';

function App() {
  return (
    <WsProvider>
      <AuthProvider>
        <MainLayout>
          <CommonDataProvider>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <OwnersTable />

              <DogsTable />
            </div>
          </CommonDataProvider>
        </MainLayout>
      </AuthProvider>
    </WsProvider>
  );
}

export default App;
