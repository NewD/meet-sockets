export const createWsConnection = (url = 'ws://localhost:8999', setIsConnected) => {
  const socket = new WebSocket(url);

  socket.onopen = function () {
    console.log('Соединение установлено.');
    setIsConnected(true);
  };

  socket.onclose = function (event) {
    setIsConnected(false);

    if (event.wasClean) {
      console.log('Соединение закрыто чисто');
    } else {
      console.log('Обрыв соединения');
    }
  };

  return socket;
};

export const closeWsConnection = (socket) => {
  socket.close();
};

export const sendMessageService = function (socket, data) {
  if (!socket?.readyState) {
    setTimeout(function () {
      sendMessageService(socket, data);
    }, 100);
  } else {
    socket.send(data);
  }
};
