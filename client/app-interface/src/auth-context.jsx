import { useContext, useState } from 'react';
import { createContext } from 'react';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [authStatus, setAuthStatus] = useState('guest');

  const signIn = (password) => {
    if (password === '12345678') {
      setAuthStatus('admin');
      return true;
    }
    setAuthStatus('error');

    return false;
  };
  const signOut = () => {
    setAuthStatus('guest');
  };

  return (
    <AuthContext.Provider value={{ authStatus, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
