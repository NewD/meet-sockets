import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from '@material-ui/core';
import { Alert, Autocomplete } from '@material-ui/lab';
import { Controller } from 'react-hook-form';

export function AddDialog({ isOpen, error, onConfirm, onClose, formProps, columns }) {
  const { control, handleSubmit, reset } = formProps;

  return (
    <Dialog
      onClick={(e) => e.stopPropagation()}
      open={isOpen}
      onClose={() => {
        onClose();
        reset();
      }}
    >
      <DialogTitle>Создание новой записи</DialogTitle>

      <form
        onSubmit={handleSubmit((data) => {
          onConfirm(data);
        })}
      >
        <DialogContent>
          <Grid spacing={2} direction="column" container>
            <Grid item> {!!error && <Alert severity="error">{error}</Alert>}</Grid>

            {columns.map(({ name, label, options }) => (
              <Grid item key={name}>
                <Controller
                  control={control}
                  name={name}
                  render={({ field }) => {
                    return options ? (
                      <Autocomplete
                        options={options}
                        {...field}
                        value={field.value || null}
                        onChange={(e, newValue) => field.onChange(newValue)}
                        getOptionLabel={(option) => option.name}
                        renderInput={(params) => (
                          <TextField
                            style={{ width: '100%' }}
                            {...params}
                            variant="outlined"
                            label={label}
                          />
                        )}
                      />
                    ) : (
                      <TextField
                        style={{ width: '100%' }}
                        {...field}
                        variant="outlined"
                        label={label}
                      />
                    );
                  }}
                />
              </Grid>
            ))}
          </Grid>
        </DialogContent>

        <DialogActions>
          <Button type="submit" color="secondary" variant="outlined">
            Добавить
          </Button>
          <Button
            onClick={() => {
              onClose();
              reset();
            }}
            color="default"
            variant="outlined"
          >
            Отмена
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}
