import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from '@material-ui/core';

export function DeleteDialog({ ids, isOpen, onConfirm, onClose }) {
  return (
    <Dialog open={isOpen} onClose={onClose}>
      <DialogTitle>Подтвердите удаление!</DialogTitle>

      <DialogContent>
        <Typography>{`Номера записей для удаления: ${ids.join(', ')}`}</Typography>
      </DialogContent>

      <DialogActions>
        <Button onClick={onConfirm} color="secondary" variant="outlined">
          Удалить
        </Button>
        <Button onClick={onClose} color="default" variant="outlined">
          Отмена
        </Button>
      </DialogActions>
    </Dialog>
  );
}
