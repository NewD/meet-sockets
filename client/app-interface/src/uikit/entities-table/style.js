import { makeStyles } from '@material-ui/core';

export const styles = makeStyles({
  tableContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    maxHeight: 400,
    position: 'relative',
  },

  tableHead: {
    position: 'sticky',
    top: 0,
    zIndex: 1,
    backgroundColor: '#d5c7ea',
  },

  tableBody: {
    maxHeight: 700,
    overflowY: 'scroll',
  },

  item: {
    marginBottom: '20px',
  },
});
