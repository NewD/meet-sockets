import React, { useState } from 'react';
import {
  Box,
  Table,
  TableHead,
  TableBody,
  TableContainer,
  TableCell,
  TableRow as MaterialRow,
  Typography,
  TextField,
} from '@material-ui/core';
import { TableRow } from './components/table-row';
import { styles } from './style';
import { useEffect } from 'react';

export const EntitiesTable = ({
  selectedRows,
  setSelectedRows,
  focusedRowKey,
  setFocusedRow,
  dataSource,
  noDataText,
  columns,
  Header,
}) => {
  const classes = styles();
  const [searchString, setSearchString] = useState('');
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    if (!searchString) {
      setFilteredData(dataSource);
    }

    const data = dataSource.filter((el) => {
      return Object.keys(el).reduce((acc, key) => {
        if (acc) return acc;

        if (!columns.find((col) => col.dataField === key)) return acc;

        return new RegExp(searchString, 'i').test(el[key]);
      }, false);
    });

    setFilteredData(data);
  }, [searchString, dataSource, columns]);

  return (
    <Box>
      {Header}
      <div style={{ marginTop: 15, marginBottom: 15 }}>
        <TextField
          disabled={dataSource.length === 0}
          variant="standard"
          label="Поиск записи"
          fullWidth
          value={searchString}
          onChange={(e) => {
            setSearchString(e.target.value);
          }}
        />
      </div>
      <TableContainer className={classes.tableContainer} component={Box}>
        <Table>
          <TableHead className={classes.tableHead}>
            <MaterialRow>
              {columns.map((col) => (
                <TableCell key={col.dataField}>
                  {col.renderHeadCell instanceof Function
                    ? col.renderHeadCell({
                        col,
                        setSelectedRows,
                        isAllSelected:
                          selectedRows.length === dataSource.length &&
                          selectedRows.length > 0,
                        dataSource,
                      })
                    : col.title}
                </TableCell>
              ))}
            </MaterialRow>
          </TableHead>
          <TableBody className={classes.tableBody}>
            {filteredData.map((item) => {
              return (
                <TableRow
                  isSelected={selectedRows.includes(item.id)}
                  setSelectedRows={setSelectedRows}
                  isFocused={item.id === focusedRowKey}
                  setFocusedRow={setFocusedRow}
                  columns={columns}
                  entity={item}
                  key={item.key}
                />
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      {dataSource.length === 0 && <Typography>{noDataText}</Typography>}
    </Box>
  );
};
