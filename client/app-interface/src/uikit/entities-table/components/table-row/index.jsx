import React from 'react';
import { TableCell, TableRow as MaterialRow } from '@material-ui/core';
import { styles } from './style';

export const TableRow = React.memo(
  ({ isSelected, setSelectedRows, isFocused, setFocusedRow, entity, columns }) => {
    const classes = styles();
    return (
      <>
        <MaterialRow
          className={classes.root}
          selected={isFocused}
          onClick={() => setFocusedRow(entity.id)}
        >
          {columns.map((col) => (
            <TableCell key={col.dataField}>
              {col.renderCell instanceof Function
                ? col.renderCell({ entity, col, setSelectedRows, isSelected })
                : entity[col.dataField]}
            </TableCell>
          ))}
        </MaterialRow>
      </>
    );
  }
);
