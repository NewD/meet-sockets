import { useEffect, useState } from 'react';
import { useCallback } from 'react';
import { createContext, useContext } from 'react';
import { useWsContext } from './ws-context';

const CommonDataContext = createContext({
  focusedRow: { table: null, key: null },
  tablesData: { dogs: [], owners: [] },
});

const initTablesData = { dogs: [], owners: [] };
const initFocusedRow = { table: null, key: null };

export const CommonDataProvider = ({ children }) => {
  const { response, isConnected } = useWsContext();
  const [focusedRow, setFocusedRow] = useState(initFocusedRow);
  const [tablesData, setTablesData] = useState(initTablesData);
  useEffect(() => {
    if (!isConnected) {
      return;
    }

    if (response?.content && (response.route === 'dogs' || response.route === 'owners')) {
      setTablesData((prev) => ({ ...prev, [response.route]: response.content }));
      if (
        focusedRow.table === response.route &&
        !response.content.find(({ id }) => id === focusedRow.key)
      ) {
        setFocusedRow(initFocusedRow);
      }
    }
  }, [response, focusedRow.table, focusedRow.key, isConnected]);

  useEffect(() => {
    if (!isConnected) {
      setTablesData(initTablesData);
      setFocusedRow(initFocusedRow);
    }
  }, [isConnected]);

  const setTableData = useCallback((tableName, data) => {
    setTablesData((prev) => ({ ...prev, [tableName]: data }));
  }, []);

  return (
    <CommonDataContext.Provider
      value={{
        focusedRow,
        setFocusedRow,
        tablesData,
        setTableData,
      }}
    >
      {children}
    </CommonDataContext.Provider>
  );
};

export const useCommonDataContext = () => useContext(CommonDataContext);
