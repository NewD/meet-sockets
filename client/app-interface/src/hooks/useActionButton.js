import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useWsContext } from '../ws-context';

export const useActionButton = (event) => {
  const { isConnected, sendMessage, response, setQueriesQuene } = useWsContext();
  const { control, handleSubmit, reset, setValue } = useForm();
  const [isOpen, setIsOpen] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (!isConnected && isOpen) {
      setIsOpen(false);
      setError(null);
      reset();
    }
  }, [isConnected, isOpen, reset]);

  useEffect(() => {
    if (!response || response.event !== event) return;

    const { error: resError, status } = response;
    if (status === 'bad' && resError) {
      console.log(response);
      setError(resError);
    }

    if (status === 'ok') {
      setIsOpen(false);
      setError(null);
      reset();
      return;
    }
  }, [response, event, reset]);

  return {
    isOpen,
    setIsOpen,
    sendMessage,
    error,
    setError,
    control,
    handleSubmit,
    reset,
    setValue,
    setQueriesQuene,
  };
};
