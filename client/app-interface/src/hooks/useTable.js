import { useEffect, useState } from 'react';
import { api } from '../api';
import { useCommonDataContext } from '../common-data-context';
import { eventsEnum } from '../events.enum';
import { useWsContext } from '../ws-context';
import { useAuth } from '../auth-context';

export const useTable = (tableName) => {
  const {
    sendMessage,
    isConnected,
    isNeedToUpdate,
    setIsNeedToUpdate,
    response,
  } = useWsContext();
  const { focusedRow, setFocusedRow, tablesData } = useCommonDataContext();
  const { authStatus } = useAuth();
  const [selectedRows, setSelectedRows] = useState([]);

  useEffect(() => {
    if (isNeedToUpdate) {
      setIsNeedToUpdate(false);
      sendMessage(api.get(tableName, tableName));
    }
  }, [isNeedToUpdate, setIsNeedToUpdate, sendMessage, tableName]);

  useEffect(() => {
    if (response?.route !== tableName) return;
    const { event } = response;

    if (event === eventsEnum.deleteRecords) {
      setSelectedRows([]);
    }
    if (event !== eventsEnum.getAll) {
      sendMessage(api.get(tableName, tableName));
      return;
    }
  }, [response, sendMessage, tableName]);

  useEffect(() => {
    if (isConnected) {
      return sendMessage(api.get(tableName, tableName));
    }
  }, [isConnected, sendMessage, tableName]);

  return {
    response,
    sendMessage,
    isConnected,
    focusedRow,
    setFocusedRow,
    selectedRows,
    setSelectedRows,
    tablesData,
    authStatus,
  };
};
