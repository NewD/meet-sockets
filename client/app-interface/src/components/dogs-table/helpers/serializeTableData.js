import { IconButton } from '@material-ui/core';
import { Edit } from '@material-ui/icons';

export const serializeTableItem = (item) => ({
  ...item,
  edit: (
    <IconButton color="primary">
      <Edit />
    </IconButton>
  ),
  key: item.id,
});

export const mapRawTabledData = (data, secondTableData, focusedRow) => {
  if (!focusedRow.table || focusedRow.key === null || focusedRow.table === 'dogs')
    return data.map(serializeTableItem);

  return data.filter(({ owner }) => focusedRow.key === owner).map(serializeTableItem);
};
