import { Paper } from '@material-ui/core';
import { useEffect } from 'react';
import { api } from '../../api';
import { eventsEnum } from '../../events.enum';
import { useTable } from '../../hooks/useTable';
import { EntitiesTable } from '../../uikit/entities-table';
import { columns } from './columns';
import { Toolbar } from './components/toolbar';
import { mapRawTabledData } from './helpers/serializeTableData';

export const dogsTableName = 'dogs';

export function DogsTable() {
  const {
    selectedRows,
    setSelectedRows,
    isConnected,
    focusedRow,
    setFocusedRow,
    tablesData,
    response,
    sendMessage,
    isOffline,
    authStatus,
  } = useTable(dogsTableName);

  useEffect(() => {
    if (!response) return;

    if (response.route === 'owners' && response.event === eventsEnum.deleteRecords) {
      sendMessage(api.get(dogsTableName, dogsTableName));
    }
  }, [response, sendMessage]);

  const dataSource = mapRawTabledData(tablesData.dogs, tablesData.owners, focusedRow);

  return (
    <Paper square elevation={7} style={{ padding: 16 }}>
      <EntitiesTable
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        focusedRowKey={focusedRow.table === dogsTableName ? focusedRow.key : null}
        setFocusedRow={(rowKey) =>
          setFocusedRow({
            table: dogsTableName,
            key: focusedRow.key === rowKey ? null : rowKey,
          })
        }
        Header={
          <Toolbar
            tablesData={tablesData}
            isDisabled={!isConnected || isOffline || authStatus !== 'admin'}
            selectedRows={selectedRows}
          />
        }
        dataSource={dataSource}
        columns={
          isOffline || authStatus !== 'admin'
            ? columns.filter(
                ({ dataField }) => dataField !== 'select' && dataField !== 'edit'
              )
            : columns
        }
        noDataText={
          isConnected ? 'Нет данных в таблице "Собаки!"' : 'Отсутствует подключение!'
        }
      />
    </Paper>
  );
}
