import { IconButton } from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { dogsTableName } from '../..';
import { api } from '../../../../api';
import { eventsEnum } from '../../../../events.enum';
import { useActionButton } from '../../../../hooks/useActionButton';
import { EditDialog } from '../../../../uikit/edit-dialog';

export function EditButton({ item }) {
  const columns = [
    { name: 'name', label: 'Кличка' },
    { name: 'age', label: 'Возраст' },
  ];

  const defaultValues = columns.reduce((acc, { name }) => {
    acc[name] = item[name];
    return acc;
  }, {});

  const {
    sendMessage,
    isOpen,
    setIsOpen,
    error,
    setError,
    control,
    handleSubmit,
    reset,
    setValue,
  } = useActionButton(eventsEnum.editRecord, defaultValues);

  return (
    <>
      <IconButton
        onClick={() => {
          setIsOpen(true);
          Object.keys(defaultValues).forEach((key) => {
            setValue(key, defaultValues[key]);
          });
        }}
      >
        <Edit />
      </IconButton>

      <EditDialog
        isOpen={isOpen}
        item={item}
        error={error}
        formProps={{ control, handleSubmit, reset }}
        onConfirm={(editedItem) => {
          sendMessage(
            api.edit(dogsTableName, {
              name: `${editedItem.name}`,
              age: editedItem.age,
              id: item.id,
            })
          );
        }}
        onClose={() => {
          setIsOpen(false);
          setError(null);
        }}
        columns={columns}
      />
    </>
  );
}
