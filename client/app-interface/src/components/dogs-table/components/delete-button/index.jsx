import { Button } from '@material-ui/core';
import { DeleteForever } from '@material-ui/icons';
import { dogsTableName } from '../..';
import { api } from '../../../../api';
import { eventsEnum } from '../../../../events.enum';
import { useActionButton } from '../../../../hooks/useActionButton';
import { DeleteDialog } from '../../../../uikit/delete-dialog';

export function DeleteButton({ ids }) {
  const { isOpen, setIsOpen, sendMessage } = useActionButton(eventsEnum.deleteRecords);

  return (
    <>
      <Button
        startIcon={<DeleteForever />}
        color="secondary"
        disabled={ids.length === 0}
        variant="contained"
        onClick={() => setIsOpen(true)}
      >
        Удалить
      </Button>

      <DeleteDialog
        isOpen={isOpen}
        ids={ids}
        onConfirm={() => sendMessage(api.delete(dogsTableName, ids), true)}
        onClose={() => {
          setIsOpen(false);
        }}
      />
    </>
  );
}
