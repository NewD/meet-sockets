import { Grid, Typography } from '@material-ui/core';
import { AddButton } from '../add-button';
import { DeleteButton } from '../delete-button';

export function Toolbar({ isDisabled, selectedRows }) {
  return (
    <Grid spacing={2} container>
      <Grid item>
        <Typography variant="h5">Таблица "Собаки"</Typography>
      </Grid>

      <Grid spacing={2} item container>
        <Grid item>
          <AddButton isDisabled={isDisabled} />
        </Grid>

        <Grid item>
          <DeleteButton ids={selectedRows} />
        </Grid>
      </Grid>
    </Grid>
  );
}
