import { Button } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';
import { dogsTableName } from '../..';
import { api } from '../../../../api';
import { useCommonDataContext } from '../../../../common-data-context';
import { eventsEnum } from '../../../../events.enum';
import { useActionButton } from '../../../../hooks/useActionButton';
import { AddDialog } from '../../../../uikit/add-dialog';

export function AddButton({ isDisabled }) {
  const {
    isOpen,
    setIsOpen,
    error,
    setError,
    sendMessage,
    control,
    handleSubmit,
    reset,
  } = useActionButton(eventsEnum.addRecord);
  const { tablesData } = useCommonDataContext();

  return (
    <>
      <Button
        startIcon={<AddBox />}
        disabled={isDisabled}
        color="primary"
        variant="contained"
        onClick={() => {
          setError(null);
          setIsOpen(true);
        }}
      >
        Добавить
      </Button>

      <AddDialog
        isOpen={isOpen}
        error={error}
        onConfirm={(newItem) => {
          const newRecord = {
            owner: newItem.owner?.id,
            name: newItem.name,
            age: newItem.age,
          };
          sendMessage(api.add(dogsTableName, newRecord));
        }}
        onClose={() => setIsOpen(false)}
        formProps={{ control, handleSubmit, reset }}
        columns={[
          { name: 'name', label: 'Кличка' },
          { name: 'age', label: 'Возраст' },
          { name: 'owner', label: 'Владелец', options: tablesData.owners },
        ]}
      />
    </>
  );
}
