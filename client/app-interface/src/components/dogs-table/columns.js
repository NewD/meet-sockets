import { Checkbox } from '@material-ui/core';
import { EditButton } from './components/edit-button';

export const columns = [
  {
    dataField: 'select',
    title: '',
    renderCell: ({ isSelected, setSelectedRows, entity }) => (
      <Checkbox
        checked={isSelected}
        onChange={(event) => {
          if (event.target.checked) {
            return setSelectedRows((prev) => [...prev, entity.key]);
          }
          setSelectedRows((prev) => prev.filter((key) => key !== entity.key));
        }}
      />
    ),
    renderHeadCell: ({ setSelectedRows, isAllSelected, dataSource }) => (
      <Checkbox
        checked={isAllSelected}
        onChange={(event) => {
          if (event.target.checked) {
            return setSelectedRows(dataSource.map((d) => d.key));
          }
          setSelectedRows([]);
        }}
      />
    ),
  },
  {
    dataField: 'id',
    title: 'ID',
  },
  {
    dataField: 'name',
    title: 'Кличка',
  },
  {
    dataField: 'age',
    title: 'Возраст',
  },
  {
    dataField: 'edit',
    title: 'Редактировать',
    renderCell: ({ entity }) => <EditButton item={entity} />,
  },
];
