import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useAuth } from '../../auth-context';

export const SignInButton = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { authStatus, signIn, signOut } = useAuth();
  const { control, handleSubmit, reset } = useForm();

  return (
    <>
      <Button
        variant="contained"
        color={authStatus === 'admin' ? 'secondary' : 'primary'}
        onClick={() => {
          if (authStatus === 'admin') {
            return signOut();
          }

          setIsOpen(true);
        }}
      >
        {authStatus === 'admin' ? 'Выйти' : 'Войти'}
      </Button>

      <Dialog
        onClick={(e) => e.stopPropagation()}
        open={isOpen}
        onClose={() => {
          setIsOpen(false);
          reset();
        }}
      >
        <DialogTitle>Вход в учетную запись администратора</DialogTitle>

        <form
          onSubmit={handleSubmit((data) => {
            if (signIn(data.password)) {
              setIsOpen(false);
              reset();
            }
          })}
        >
          <DialogContent>
            <Grid spacing={2} direction="column" container>
              <Grid item>
                {authStatus === 'error' && (
                  <Alert severity="error">Неверный пароль!</Alert>
                )}
              </Grid>
              <Grid item>
                <Controller
                  control={control}
                  name="password"
                  render={({ field }) => (
                    <TextField
                      type="password"
                      style={{ width: '100%' }}
                      {...field}
                      variant="outlined"
                      label="Пароль"
                    />
                  )}
                />
              </Grid>
            </Grid>
          </DialogContent>

          <DialogActions>
            <Button type="submit" color="secondary" variant="outlined">
              Войти
            </Button>
            <Button
              onClick={() => {
                signOut();
                setIsOpen(false);
                reset();
              }}
              color="default"
              variant="outlined"
            >
              Отмена
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  );
};
