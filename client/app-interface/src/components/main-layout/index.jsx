import { Box, Container } from '@material-ui/core';
import { Menu } from '../menu';
import { SignInButton } from '../sign-in-button';

export function MainLayout({ children }) {
  return (
    <Container>
      <Box
        style={{
          marginBottom: 16,
          width: '100%',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Menu />

        <SignInButton />
      </Box>
      {children}
    </Container>
  );
}
