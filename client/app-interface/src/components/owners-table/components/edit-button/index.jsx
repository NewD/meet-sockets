import { IconButton } from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { ownersTableName } from '../..';
import { api } from '../../../../api';
import { eventsEnum } from '../../../../events.enum';
import { useActionButton } from '../../../../hooks/useActionButton';
import { EditDialog } from '../../../../uikit/edit-dialog';

const columns = [{ name: 'name', label: 'Имя' }];

export function EditButton({ item }) {
  const defaultValues = columns.reduce((acc, { name }) => {
    acc[name] = item[name];
    return acc;
  }, {});

  const {
    sendMessage,
    isOpen,
    setIsOpen,
    error,
    setError,
    control,
    handleSubmit,
    reset,
    setValue,
  } = useActionButton(eventsEnum.editRecord, defaultValues);

  return (
    <>
      <IconButton
        onClick={() => {
          setIsOpen(true);
          Object.keys(defaultValues).forEach((key) => {
            setValue(key, defaultValues[key]);
          });
        }}
      >
        <Edit />
      </IconButton>

      <EditDialog
        isOpen={isOpen}
        item={item}
        error={error}
        formProps={{ control, handleSubmit, reset }}
        onConfirm={(editedItem) => {
          sendMessage(
            api.edit(ownersTableName, {
              name: `${editedItem.name}`,
              id: item.id,
            })
          );
        }}
        onClose={() => {
          setIsOpen(false);
          setError(null);
        }}
        columns={columns}
      />
    </>
  );
}
