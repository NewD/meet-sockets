import { Button } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';
import { ownersTableName } from '../..';
import { api } from '../../../../api';
import { eventsEnum } from '../../../../events.enum';
import { useActionButton } from '../../../../hooks/useActionButton';
import { AddDialog } from '../../../../uikit/add-dialog';

export function AddButton({ isDisabled }) {
  const {
    isOpen,
    setIsOpen,
    error,
    setError,
    sendMessage,
    control,
    handleSubmit,
    reset,
  } = useActionButton(eventsEnum.addRecord);

  return (
    <>
      <Button
        startIcon={<AddBox />}
        disabled={isDisabled}
        color="primary"
        variant="contained"
        onClick={() => setIsOpen(true)}
      >
        Добавить
      </Button>

      <AddDialog
        isOpen={isOpen}
        error={error}
        onConfirm={(newItem) =>
          sendMessage(
            api.add(ownersTableName, {
              name: `${newItem.name}`,
            })
          )
        }
        onClose={() => {
          setIsOpen(false);
          setError(null);
        }}
        formProps={{ control, handleSubmit, reset }}
        columns={[{ name: 'name', label: 'Имя' }]}
      />
    </>
  );
}
