import { IconButton } from '@material-ui/core';
import { Edit } from '@material-ui/icons';

export const serializeTableItem = (item) => ({
  ...item,
  edit: (
    <IconButton color="primary">
      <Edit />
    </IconButton>
  ),
  key: item.id,
});

export const mapRawTabledData = (data, secondTableData, focusedRow) => {
  if (!focusedRow.table || focusedRow.key === null || focusedRow.table === 'owners')
    return data.map(serializeTableItem);

  const availableIds = secondTableData
    .filter(({ id }) => id === focusedRow.key)
    .map(({ owner }) => owner);

  return data.filter(({ id }) => availableIds.includes(id)).map(serializeTableItem);
};
