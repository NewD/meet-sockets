import { Paper } from '@material-ui/core';
import { useTable } from '../../hooks/useTable';
import { EntitiesTable } from '../../uikit/entities-table';
import { columns } from './columns';
import { Toolbar } from './components/toolbar';
import { mapRawTabledData } from './helpers/serializeTableData';

export const ownersTableName = 'owners';

export function OwnersTable() {
  const {
    selectedRows,
    setSelectedRows,
    isConnected,
    focusedRow,
    setFocusedRow,
    tablesData,
    isOffline,
    authStatus,
  } = useTable(ownersTableName);
  const dataSource = mapRawTabledData(tablesData.owners, tablesData.dogs, focusedRow);

  return (
    <Paper square elevation={7} style={{ padding: 16 }}>
      <EntitiesTable
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        focusedRowKey={focusedRow.table === ownersTableName ? focusedRow.key : null}
        setFocusedRow={(rowKey) =>
          setFocusedRow({
            table: ownersTableName,
            key: focusedRow.key === rowKey ? null : rowKey,
          })
        }
        Header={
          <Toolbar
            isDisabled={!isConnected || isOffline || authStatus !== 'admin'}
            selectedRows={selectedRows}
          />
        }
        dataSource={dataSource}
        columns={
          isOffline || authStatus !== 'admin'
            ? columns.filter(
                ({ dataField }) => dataField !== 'select' && dataField !== 'edit'
              )
            : columns
        }
        noDataText={
          isConnected ? 'Нет данных в таблице "Люди!"' : 'Отсутствует подключение!'
        }
      />
    </Paper>
  );
}
