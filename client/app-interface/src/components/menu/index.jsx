import { Box, Switch, Typography } from '@material-ui/core';
import { useWsContext } from '../../ws-context';

export function Menu() {
  const { isConnected, connect, disconnect } = useWsContext();

  const handleChange = (event) => {
    const isChecked = event.target.checked;
    if (isChecked) connect();
    else disconnect();
  };

  return (
    <Box>
      <Typography variant="h6">Состояние подключения</Typography>

      <Box style={{ display: 'flex', alignContent: 'center' }}>
        <Typography style={{ paddingTop: 8 }}>Отключено</Typography>

        <Switch checked={isConnected} onChange={handleChange} color="primary" />

        <Typography style={{ paddingTop: 8 }}>Подключено</Typography>
      </Box>
    </Box>
  );
}
