const http = require('http');
const express = require('express');
const WebSocket = require('ws');
const net = require('net');

const port = 9999;
const host = '192.168.1.215';

const connectTcp = () => {
  const tcpSocket = net.createConnection(port, host, function () {
    console.log('DB connected!');
  });

  tcpSocket.setEncoding('utf8');

  tcpSocket.on('end', () => {
    console.log('disconnected');
  });

  tcpSocket.on('error', (err) => {
    console.log(err);
  });

  return tcpSocket;
};

const app = express();

const server = http.createServer(app);

const webSocketServer = new WebSocket.Server({ server });

webSocketServer.on('connection', async (ws) => {
  console.log('Web socket connected successfully!');
  const tcpSocket = connectTcp();

  ws.on('message', (message) => {
    tcpSocket.write(message);
  });

  tcpSocket.on('data', (data) => {
    ws.send(data);
  });

  ws.on('close', () => {
    console.log('Disconnected from server!');
    tcpSocket.destroy();
  });

  ws.on('error', (e) => ws.send(e));
});

server.listen(8999, () => console.log('Client started'));
